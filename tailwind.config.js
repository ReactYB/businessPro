/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme");
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],

  theme: {
    extend: {
      fontFamily: {
        sans: ["Lato", ...defaultTheme.fontFamily.sans],
      },
      screens: {
        "3xl": "1440px",
      },
      colors: {
        G9Green: "#00D78E",
        G9Gray: "#1E192E",
        G9LightGrey: "#F5F6F6",
      },
    },
  },

  plugins: [],
};
