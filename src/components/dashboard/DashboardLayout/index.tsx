import React from "react";
import Drawer from "../Drawer";
interface Props {
  children: React.ReactNode;
}
const DashboardLayout = ({ children }: Props) => {
  
  return (
    <div className="flex h-screen p-1 md:p-4">
      <Drawer />
      <div className="w-full h-full px-1 overflow-auto md:px-4">
        {" "}
        {children}
      </div>
    </div>
  );
};

export default DashboardLayout;
 