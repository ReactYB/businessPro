import React from "react";

const Loading = () => {
  return (
    <div className="flex items-center justify-center w-full mt-48 -ml-24">
      <div className="relative">
        <div className="absolute w-20 h-20 border-4 border-gray-200 border-solid rounded-full"></div>
        <div className="absolute w-20 h-20 border-4 border-solid rounded-full border-G9Green animate-spin border-t-transparent"></div>
      </div>
    </div>
  );
};

export default Loading;