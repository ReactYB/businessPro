"use client";
import { Bookmark, BusinessChat, Logout, Save } from "@/components/icons";
import React from "react";
import Image from "next/image";
import NavLink from "../NavLink";
import { signOut, useSession } from "next-auth/react";

const Drawer = () => {
  const {data} = useSession();
  const links = [
    {
      label: "Appel d'offre",
      path: "/dashboard",
      targetSegment: null,
      icon: <BusinessChat />,
    },
    {
      label: "Archives",
      path: "/dashboard/archive",
      targetSegment: "archive",
      icon: <Save />,
    },
    {
      label: "Sauvgardes",
      path: "/dashboard/bookmark",
      targetSegment: "bookmark",
      icon: <Bookmark />,
    },
  ];

  return (
    <aside className="md:flex flex-col hidden md:w-2/6 xl:w-3/12 h-full rounded-2xl overflow-y-auto bg-G9Gray bg-[url('/images/effet.svg')] bg-no-repeat bg-right-bottom">
      <div className="text-G9Green text-base sm:text-xl xl:text-3xl font-bold h-40 flex items-center justify-center bg-[url('/images/Taieri.svg')] bg-no-repeat bg-cover bg-center">
        BusinessPro
      </div>
      <div className="flex items-center px-2 py-4 mt-1">
        <Image
          width={40}
          height={40}
          className="object-cover w-6 h-6 mx-2 bg-white rounded-full md:h-10 md:w-10"
          src={data?.user?.image ? data?.user?.image : "/images/Gear9Logo.png"}
          alt="avatar"
        />
        <div className="flex flex-col truncate">
          <h4 className="px-2 text-sm font-medium text-gray-800 truncate xl:text-base dark:text-gray-200">
            {data?.user?.name}
          </h4>
          <p className="px-2 text-xs font-medium text-gray-600 truncate xl:text-sm dark:text-gray-400">
            {data?.user?.email}
          </p>
        </div>
      </div>
      <div className="flex flex-col justify-between flex-1 px-2 py-4 mt-2 lg:px-4">
        <nav>
          {links.map((link) => (
            <NavLink key={link.targetSegment} NavLink={link} />
          ))}
        </nav>
        <button
          className="flex items-center px-4 py-2 mt-auto mb-16 text-gray-200 transition-colors duration-300 rounded-lg group hover:text-red-600"
          onClick={()=>signOut()}
        >
          <div className="text-gray-500 transition-colors duration-300 group-hover:text-red-600 ">
            <Logout />
          </div>
          <span className="px-2 text-sm font-medium truncate xl:text-base md:px-4">
            Déconnexion
          </span>
        </button>
        <a
          className="flex flex-col py-2 text-gray-200 group bg-[url('/images/Gear9Logo.png')] bg-no-repeat bg-left-bottom bg-[length:85px]"
          href="https://www.gear9.ma/"
          rel="noopener noreferrer"
          target="_blank"
        ></a>
      </div>
    </aside>
  );
};

export default Drawer;
