import { Bookmark, Download, Save } from "@/components/icons";
import React, { useState } from "react";

 

const TableRow =  ({ offre } : any ) => {
  const [ActiveButton, setActiveButton] = useState(false);
  return (
    <>
      <td className="p-2 text-center">
        <div className="inline-flex flex-col gap-2">
          <button
            className={`${
              ActiveButton
                ? "!bg-G9Green text-white hover:!bg-red-500"
                : " hover:!bg-G9Green"
            } p-2 rounded-full group-hover:text-white group-hover:bg-gray-300 bg-white hover:!text-white transition-colors duration-250`}
            onClick={() => setActiveButton((prev) => !prev)}
          >
            <Bookmark />
          </button>
          <button className="p-2 bg-white rounded-full group-hover:text-white group-hover:bg-gray-300 hover:!text-white hover:!bg-G9Green transition-colors duration-250">
            <Save />
          </button>
        </div>
      </td>
      <td className="p-2 text-left min-w-[140px]  ">
        <div className="flex flex-col gap-2 ">
          <p className="">{offre.date_pub}</p>
          <p className="">{offre.acheteur}</p>
        </div>
      </td>
      <td className="p-2 my-auto min-w-[240px]">
        {offre.objet_complet}
      </td>
      <td className="p-2 text-left">
        <span className="">{offre.lieu}</span>
      </td>
      <td className="p-2 text-left">{offre.date_remise}</td>
      <td className="p-3 ">10 000,00 Dhs</td>
      <td className="p-3 ">648 000,00 Dhs</td>
      <td className="p-3 text-center">
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://drive.google.com/file/u/4/d/1joVbk3D4O1uo3yaiOf6bo4zjxCvfGwxg/view?usp=drivesdk"
          className="inline-block p-2 rounded-full hover:!text-G9Green hover:!bg-white"
          download
        >
          <Download />
        </a>
      </td>
    </>
  );
};

export default TableRow;
