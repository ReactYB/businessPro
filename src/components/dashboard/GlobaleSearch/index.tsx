import { SearchIcon } from "@/components/icons";
import { useSession } from "next-auth/react";
import React, { useEffect, useState } from "react";

const GlobaleSearch = ({
  value: initialValue,
  onChange,
  tableName,
  debounce = 1000,
  ...props
}: {
  value: string | number;
  onChange: (value: string | number) => void;
  tableName: string;
  debounce?: number;
} & Omit<React.InputHTMLAttributes<HTMLInputElement>, "onChange">) => {
  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      onChange(value);
      console.log(value);
    }, debounce);

    return () => clearTimeout(timeout);
  }, [debounce, onChange, value]);

  const { data,status} = useSession();

  return (
    <div className="py-8 md:pl-8">
      <div className="flex flex-wrap md:flex-nowrap">
        <div className="mx-auto md:mx-0 md:w-8/12">
          <div className="text-xl text-center md:text-3xl xl:text-4xl sm:text-left">
            Heureux de vous revoir,{" "}
            <span className="font-bold">
              {status === "loading" ? "" : data?.user?.name}
            </span>
          </div>
          <div className="text-sm text-center lg:text-base sm:text-left">
            Vous êtes actuellement sur la page,
            <span className="font-semibold text-G9Green"> {tableName}</span>
          </div>
        </div>
        <div className="mx-auto md:ml-auto md:mr-0 md:4/12">
          <div className="relative flex items-center w-full h-12 mt-4 bg-white border rounded-lg focus-within:shadow-lg md:mt-0">
            <div className="grid w-12 h-full text-gray-500 place-items-center">
              <SearchIcon />
            </div>
            <input
              {...props}
              className="w-full h-full pr-2 text-sm text-gray-700 outline-none peer"
              type="text"
              id="search"
              placeholder="Search..."
              value={value}
              onChange={(e) => setValue(e.target.value)}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default GlobaleSearch;
