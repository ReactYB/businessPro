import {
  getCoreRowModel,
  useReactTable,
  flexRender,
  getPaginationRowModel,
  SortingState,
  getSortedRowModel,
  FilterFn,
  getFilteredRowModel,
} from "@tanstack/react-table";
import type { ColumnDef } from "@tanstack/react-table";
import TableRow from "../TableRow";
import { ArrowBis, ArrowLast, ArrowStart } from "@/components/icons";
import { useState } from "react";
import { rankItem } from "@tanstack/match-sorter-utils";
import GlobaleSearch from "../GlobaleSearch";
declare module "@tanstack/table-core" {
  interface FilterFns {
    Globale: FilterFn<unknown>;
  }
}
//
// https://dev.to/esponges/create-a-reusable-react-table-component-with-typescript-56d4
// A debounced input react component
interface ReactTableProps<T extends object> {
  data: T[];
  columns: ColumnDef<T>[];
  showNavigation?: boolean;
  tableName: "Appels d’Offres" | "Archives" | "Sauvgardes";
}

// Custom Filter Function
const GlobaleFilter: FilterFn<any> = (row, columnId, value, addMeta) => {
  // Rank the item
  const itemRank = rankItem(row.getValue(columnId), value);
  // Store the itemRank info
  addMeta({
    itemRank,
  });
  // Return if the item should be filtered in/out
  return itemRank.passed;
};

const Table = <T extends object>({
  data,
  columns,
  showNavigation,
  tableName
}: ReactTableProps<T>) => {
  // Sorting State
  const [sorting, setSorting] = useState<SortingState>([]);
  // Filter State
  const [globalFilter, setGlobalFilter] = useState("");
  // Table State
  const table = useReactTable({
    data,
    columns,
    filterFns: {
      Globale: GlobaleFilter,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      pagination: {
        // initial item per page
        pageSize: 5,
      },
    },
    state: {
      // definir state de sorting deja declarer
      sorting,
      globalFilter,
    },
    onSortingChange: setSorting,
    getSortedRowModel: getSortedRowModel(),
    onGlobalFilterChange: setGlobalFilter,
    getFilteredRowModel: getFilteredRowModel(),
  });

  return (
    <>
      {" "}
      <div>
        <GlobaleSearch
          value={globalFilter ?? ""}
          onChange={(value) => setGlobalFilter(String(value))}
          className="p-2 border shadow font-lg border-block"
          placeholder="Search all columns..."
          tableName={tableName}
        />
      </div>
      <div className="w-full px-1 overflow-auto md:px-4 no-scrollbar">
        <div className="G9-table no-scrollbar ">
          <table className="table w-full text-sm text-gray-400">
            <thead className="relative z-50 text-xs font-semibold bg-white text-G9Gray">
              {/* Maping sur les rows columns header */}
              {table.getHeaderGroups().map((headerGroup) => (
                <tr key={headerGroup.id}>
                  {/* Maping sur les columns header */}
                  {headerGroup.headers.map((header) => (
                    <th
                      key={header.id}
                      className="sticky top-0 px-4 py-2 text-left bg-white"
                    >
                      {header.isPlaceholder ? null : (
                        <div
                          {...{
                            // EnableSorting: false/true, dans les columns
                            className: header.column.getCanSort()
                              ? "flex items-center cursor-pointer select-none hover:underline "
                              : "",
                            // Switch entre sorting state
                            onClick: header.column.getToggleSortingHandler(),
                          }}
                        >
                          {flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                          {{
                            asc: (
                              <ArrowBis
                                style={{
                                  transform: "rotate(90deg)",
                                  marginLeft: "auto",
                                  width: "12px",
                                  height: "12px",
                                }}
                              />
                            ),
                            desc: (
                              <ArrowBis
                                style={{
                                  transform: "rotate(-90deg)",
                                  fontWeight: "900",
                                  marginLeft: "auto",
                                  width: "12px",
                                  height: "12px",
                                }}
                              />
                            ),
                          }[header.column.getIsSorted() as string] ?? (
                            <>
                              {header.column.getCanSort() && (
                                // default arrow
                                <ArrowBis
                                  style={{
                                    marginLeft: "auto",
                                    width: "12px",
                                    height: "12px",
                                  }}
                                />
                              )}
                            </>
                          )}
                        </div>
                      )}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody>
              {table.getRowModel().rows.map((row) => (
                <tr key={row.id} className="text-xs group hover:text-G9Gray">
                  <TableRow offre={row.original} />
                </tr>
              ))}
            </tbody>
          </table>
          {showNavigation && (
            <>
              <div className="flex items-center justify-end gap-2 text-sm text-gray-400">
                <button
                  className="hover:text-G9Gray"
                  onClick={() => table.setPageIndex(0)}
                  disabled={!table.getCanPreviousPage()}
                >
                  <ArrowStart />
                </button>
                <button
                  className={`${
                    !table.getCanPreviousPage() ? "hidden " : " "
                  } w-8 h-8  bg-gray-100 rounded-full hover:bg-G9Gray hover:text-white`}
                  onClick={() => table.previousPage()}
                  disabled={!table.getCanPreviousPage()}
                >
                  {table.getState().pagination.pageIndex}
                </button>
                <button
                  className="w-8 h-8 text-white rounded-full bg-G9Green hover:bg-G9Gray"
                  disabled={!table.getCanNextPage()}
                >
                  {table.getState().pagination.pageIndex + 1}
                </button>
                <button
                  className={`${
                    !table.getCanNextPage() ? "hidden " : " "
                  } w-8 h-8  bg-gray-100 rounded-full hover:bg-G9Gray hover:text-white`}
                  onClick={() => table.nextPage()}
                  disabled={!table.getCanNextPage()}
                >
                  {table.getState().pagination.pageIndex + 2}
                </button>
                <button
                  className={` hover:text-G9Gray`}
                  onClick={() => table.setPageIndex(table.getPageCount() - 1)}
                  disabled={!table.getCanNextPage()}
                >
                  <ArrowLast />
                </button>
                <span className="flex items-center gap-1 cursor-pointer">
                  <div>Page</div>
                  <strong>
                    {table.getState().pagination.pageIndex + 1} of{" "}
                    {table.getPageCount()}
                  </strong>
                </span>
                {/* <span className="flex items-center gap-1">
                | Go to page:
                <input
                  type="number"
                  defaultValue={table.getState().pagination.pageIndex + 1}
                  onChange={(e) => {
                    const page = e.target.value
                      ? Number(e.target.value) - 1
                      : 0;
                    table.setPageIndex(page);
                  }}
                  className="w-16 p-1 border rounded"
                />
              </span> */}
                {/* <select
                value={table.getState().pagination.pageSize}
                onChange={(e) => {
                  table.setPageSize(Number(e.target.value));
                }}
              >
                {[2, 4, 6, 8, 50].map((pageSize) => (
                  <option key={pageSize} value={pageSize}>
                    Show {pageSize}
                  </option>
                ))}
              </select> */}
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Table;
