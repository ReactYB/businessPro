"use client";
import { ArrowBis } from "@/components/icons";
import { INavLinkType } from "@/types/types";
import Link from "next/link";
import { useRouter } from "next/router";

interface Props {
  NavLink: INavLinkType;
}

const NavLink = ({ NavLink }: Props) => {
  const router = useRouter();   
  return (
    <Link
      className={`${
        router.pathname == NavLink.path
          ? "text-G9Green bg-gray-800 "
          : "text-gray-200 "
      } group flex items-center mb-2 px-2 xl:px-4 py-2 transition-colors duration-300 transform rounded-lg hover:bg-gray-800 hover:text-G9Green`}
      href={NavLink.path}
    >
      <div
        className={`${
          router.pathname == NavLink.path ? "text-G9Green " : "text-gray-500 "
        }group-hover:text-G9Green  transition-colors duration-300`}
      >
        {NavLink.icon}
      </div>
      <span className="px-2 text-sm font-medium truncate xl:text-base md:px-4">
        {NavLink.label}
      </span>
      <ArrowBis style={{ marginLeft: "auto" }} />
    </Link>
  );
};
export default NavLink;
