import React from "react";

const Triangle = ({ style = {} }) => {
  return (
    <svg width="39" height="41" viewBox="0 0 39 41" fill="none" style={style}>
      <path
        d="M0.466309 40.335V0.647644H38.3497L0.466309 40.335Z"
        fill="#F5F7F7"
      />
    </svg>
  );
};

export default Triangle;
