import "@/styles/globals.min.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { IProps } from "@/types/types";
import {
  Hydrate,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import React from "react";
import { SessionProvider } from "next-auth/react";

export default function App({ Component, pageProps }: IProps) {
  const Layout = Component.Layout || ((page: React.ReactNode) => page);
  // Create Query client
  const [queryClient] = React.useState(() => new QueryClient());

  //
  // https://stackoverflow.com/questions/70297964/next-js-how-to-prevent-flash-of-the-unauthorized-route-page-prior-to-redirect-w
  //
  return (
    <SessionProvider session={pageProps}>
      <QueryClientProvider client={queryClient}>
        <Hydrate state={pageProps.dehydratedState}>
          {Layout(
            <>
              <Component {...pageProps} />
              <ToastContainer />
            </>
          )}
        </Hydrate>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </SessionProvider>
  );
}
