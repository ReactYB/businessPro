import NextAuth, { getServerSession } from "next-auth";
import GoogleProvider from "next-auth/providers/google";
// For more information on each option (and a full list of options) go to
// https://next-auth.js.org/configuration/options

const authOptions = {
  // https://next-auth.js.org/configuration/providers
  providers: [
    GoogleProvider({
      // @ts-ignore
      clientId: process.env.BUSINESSPRO_GOOGLE_ID,
      // @ts-ignore
      clientSecret: process.env.BUSINESSPRO_GOOGLE_SECRET,
      authorization: {
        params: {
          prompt: "consent",
          access_type: "offline",
          response_type: "code"
        }
      },
    }),
  ],
  // Only Gear9ers To log in
  callbacks: {
    async signIn({ account, profile }: any) {
      if (account.provider === "google") {
        return profile.email_verified && profile.email.endsWith("@gear9.ma")
      }
    },
  },
  secret: process.env.NEXT_AUTH_SECRET
}
export const getServerAuthSession = (context: any) => { return getServerSession(context.req, context.res, authOptions) }
export default NextAuth(authOptions);