import {
  DashboardLayout,
  ErrorTable,
  Loading,
  Table,
} from "@/components/dashboard";
import AuthHOC from "@/components/dashboard/AuthHOC";
import useGetOffres from "@/services/allOffres";
import { IOffreType } from "@/types/types";
import { ColumnDef } from "@tanstack/react-table";
import React, { useMemo } from "react";

const Bookmark = () => {
  // Table Column des Archives Sauvgarder
  const columns = useMemo<ColumnDef<IOffreType>[]>(
    () => [
      {
        header: "Publié",
        accessorKey: "",
        enableSorting: false,
      },
      {
        header: "N° / Entité",
        accessorKey: "acheteur",
        enableSorting: true,
      },
      {
        header: "Objet",
        accessorKey: "objet_complet",
        enableSorting: true,
      },
      {
        header: "Lieu d’exécution",
        accessorKey: "lieu",
        enableSorting: true,
      },
      {
        header: "Dépôt",
        accessorKey: "date_remise",
        enableSorting: true,
      },
      {
        header: "Caution",
        accessorKey: "objet",
        enableSorting: true,
      },
      {
        header: "Valeur",
        accessorKey: "link",
        enableSorting: true,
      },
      {
        header: "Dossier",
        accessorKey: "link_download",
        enableSorting: false,
      },
    ],
    []
  );
  const { isLoading, data, error } = useGetOffres();
  if (isLoading) return <Loading />;
  if (error) return <ErrorTable />;
  return <>{data && <Table tableName="Sauvgardes" data={data} columns={columns} showNavigation />}</>;
};

export default Bookmark;

// Adding Layout per/page
Bookmark.Layout = function Layout(page: React.ReactNode) {
  return (
    <AuthHOC>
      <DashboardLayout>{page}</DashboardLayout>
    </AuthHOC>
  );
};
