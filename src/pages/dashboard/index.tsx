import React, { useMemo } from "react";
import Table from "@/components/dashboard/Table";
import { AuthHOC, DashboardLayout, ErrorTable, Loading } from "@/components/dashboard";
import useGetOffres from "@/services/allOffres";
import { ColumnDef } from "@tanstack/react-table";
import { IOffreType } from "@/types/types";


const Dashboard = () => {
  // Table Column des Offre Now
  const columns = useMemo<ColumnDef<IOffreType>[]>(
    () => [
      {
        header: "Publié",
        accessorKey: "",
        enableSorting: false,
      },
      {
        header: "N° / Entité",
        accessorKey: "acheteur",
        enableSorting: true,
      },
      {
        header: "Objet",
        accessorKey: "objet_complet",
        enableSorting: true,
      },
      {
        header: "Lieu d’exécution",
        accessorKey: "lieu",
        enableSorting: true,
      },
      {
        header: "Dépôt",
        accessorKey: "date_remise",
        enableSorting: true,
      },
      {
        header: "Caution",
        accessorKey: "objet",
        enableSorting: true,
      },
      {
        header: "Valeur",
        accessorKey: "link",
        enableSorting: true,
      },
      {
        header: "Dossier",
        accessorKey: "link_download",
        enableSorting: false,
      },
    ],
    []
  );
  const { isLoading, data, error } = useGetOffres();
  if (isLoading) return <Loading />;
  if (error) return <ErrorTable />;
  return <>{data && <Table tableName="Appels d’Offres" data={data} columns={columns} showNavigation />}</>;
};

export default Dashboard;

// Adding Layout per/page
Dashboard.Layout = function Layout(page: React.ReactNode) {
  return (
    <AuthHOC>
      <DashboardLayout>{page}</DashboardLayout>
    </AuthHOC>
  );
};
