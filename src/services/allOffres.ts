import { useQuery } from "@tanstack/react-query";
import { IOffreType } from "@/types/types";
import agent from "./api/api";
import { AxiosError } from "axios";

export default function useGetOffres() {
    return useQuery<any, AxiosError, Array<IOffreType>, Array<string>>({
        queryKey: ["/getAllOffre"],
        async queryFn() {
            const response = await agent.Offre.list();
            return response;
        },
    });
}
