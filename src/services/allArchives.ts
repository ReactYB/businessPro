import { useQuery } from "@tanstack/react-query";
import { IOffreType } from "@/types/types";
import agent from "./api/api";
import { AxiosError } from "axios";

export default function useGetArchives() {
    return useQuery<any, AxiosError, Array<IOffreType>, Array<string>>({
        queryKey: ["/getAllArchive"],
        async queryFn() {
            const response = await agent.Archive.list();
            return response;
        },
    });
}
