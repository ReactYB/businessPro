import axios, { AxiosError, AxiosResponse } from "axios";
import { toast } from "react-toastify";

// default url setup
axios.defaults.baseURL = "http://localhost:5555/";
axios.defaults.withCredentials = false;

const responseBody = (response: AxiosResponse) => response.data;

// Time out to delay response
const sleep = () => new Promise((resolve) => setTimeout(resolve, 2500));

// Axios Response interceptors
axios.interceptors.response.use(
  async (response) => {
    await sleep();
    return response;
  },
  (error: AxiosError) => {
    // TODO Need error format back end to config
    const { status } = error.response! as AxiosResponse;
    switch (status) {
      case 400:
        toast.error(error.code);
        break;
      case 401:
        toast.error(error.code);
        break;
      case 404:
        toast.error(error.code);
        break;
      case 500:
        toast.error(error.code);
        break;
      default:
        break;
    }
    return Promise.reject(error.response?.data);
  } // Adding return pour ne pas crash
);

// TODO Axios Response interceptors
axios.interceptors.request.use((config) => {
  const token = "Token frm store";
  if (token) config.headers.Authorization = `Bearer ${token}`;
  return config;
});

// Generique Request setup
const requests = {
  // UrlSearchParams pour passer les params
  get: (url: string, params?: URLSearchParams) =>
    axios.get(url, { params }).then(responseBody),
  post: (url: string, body: {}) => axios.post(url).then(responseBody),
  put: (url: string, body: {}) => axios.put(url).then(responseBody),
  delete: (url: string) => axios.delete(url).then(responseBody),
};
// Offre requests
// TODO post from offre to archive ( no backend yet )
const Offre = {
  list: (params?: URLSearchParams) => requests.get("getAllOffre", params),
  details: (id: number) => requests.get(`getOffre/${id}`),
};
// TODO post from archive to offre( no backend yet )
const Archive = {
  list: (params?: URLSearchParams) => requests.get("getAllArchive", params),
  details: (id: number) => requests.get(`getArchive/${id}`),
};
// TODO post from offre to Bookmark ( no backend yet )
const Bookmark = {
  list: (params?: URLSearchParams) => requests.get("getAllBookmark", params),
  details: (id: number) => requests.get(`getBookmark/${id}`),
};

const agent = {
  Offre,
  Archive,
  Bookmark,
};

export default agent;
