import { NextPage } from "next";

export interface IOffreType {
  id: number
  date_pub: string
  reference: string
  objet_complet: string
  objet: any
  acheteur: string
  lieu: string
  date_remise: string
  link: string
  link_download: string
}

export interface INavLinkType {
  label: string,
  path : string,
  targetSegment?:string | null,
  icon: any
}

// Page type
export type IPage<P = {}> = NextPagextPage<P> & {
  Layout?: (page: React.ReactNode) => React.ReactNode;
};
// Expanding AppProps type
export type IProps = AppProps & {
  Component: Page;
};